# Identity

!!! abstract

    This page lists standards relating to identity within the University.
    We use "identity" to refer to information on a University member relating to
    themselves and their role in the University.

    This page is useful for all those involved in service delivery, procurement
    or co-ordination.

The University Information Services provides with a
[directory service](https://www.lookup.cam.ac.uk/) containing information about
every university member and institution. This service can be accessed either via
LDAP or a [bespoke
API](https://help.uis.cam.ac.uk/service/collaboration/lookup/ws). Both LDAP and
API access is authenticated using Lookup group credentials.

Data in Lookup is sourced from a number of different databases including our
Student Information System and HR System. Some information is user-editable and
should not be considered "authoritative".

!!! tip

    The [Lookup
    documentation](https://help.uis.cam.ac.uk/service/collaboration/lookup/using)
    is the most up-to-date source on what information is user-editable in
    Lookup.

Lookup also hosts self-service group management and support nested groups.
Lookup group membership can be used for access control purposes.

!!! danger

    Users can hide their group membership if they want. Make sure that API calls
    are authenticated as the access control group or, if this is not possible,
    ensure that membership of the group *adds* privilege rather than removing
    it. I.e. do not use membership of a group to *deny* access.

Applications should use Lookup to retrieve identity or group information about
University members and institutions.

## Other usage scenarios

The Technical Design Authority (TDA) understands that identity is a complex and
wide-ranging topic. A particular service's use case may not be covered by these
standards. Is this is the case, contact the TDA for further guidance and
support.
