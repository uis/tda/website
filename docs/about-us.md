# About the TDA

This page provides some information on who makes up the TDA and what it does.

## Who

The Technology Design Authority is currently composed of the following members:

This section will be updated soon

## What

The TDA has three key roles:

* to own the set of technical standards UIS should use across its systems,
    projects and services,
* to provide technical advice, and
* to approve technical aspects of all high-level project designs.

## Terms of reference

By agreement with senior UIS management, the Technical Design Authority shall:

* consist of at least the Architecture team and the Portfolio Technical Leads,
* own the set of technical standards projects shall choose from for all new
  services, and the development of existing ones,
* provide guidance on technical decisions,
* approve or reject high-level designs as early as possible in the project life
  cycle and as required later, and
* receive input from all portfolios via the Portfolio Technical Leads, and feed
   technical advice back.

## Contact

You can contact the TDA by emailing
[tech-design@uis.cam.ac.uk](mailto:tech-design@uis.cam.ac.uk).
