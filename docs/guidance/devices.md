# End-user device support

!!! abstract

    This page provides guidance on what end-user devices should be supported by
    UIS services. We restrict the guidance to services intended to be consumed
    directly by end-users via either a website or an installed app.

    This page is suitable for anyone involved in service delivery, procurement
    or co-ordination.

The Technical Design Authority recommends that the current Raven usage
statistics be used as a guide to end-user devices used by members of the
University. When procuring services, ensure that the maximum number of end-user
devices are supported.

Ideally services will be Operating System agnostic; prefer services delivered
over the Web to services which can only be accessed via a locally deployed
application.

When procuring services which are available both over the Web and via a local
app, be clear what functionality is present in both scenarios. Evaluate the
product based on the *intersection* of the functionality, not the *union*.

## Raven end-user device statistics

This section contains device usage statistics based on recorded visits to
[https://raven.cam.ac.uk/](https://raven.cam.ac.uk/). It can be used to get a
rough insight into what devices are commonly used by University members. The
data is provided by the [Raven device statistics
API](https://developer.api.apps.cam.ac.uk/docs/ravenstats/1/overview).

<div id="ravenStats">
  Unfortunately it appears that your browser doesn't support our device API.
</div>

<script>
(async () => {
  try {
    const statesEl = document.getElementById('ravenStats');
    statesEl.innerText = 'Loading data';
    const data = await (await fetch('https://api.apps.cam.ac.uk/ravenstats')).json();
    statesEl.innerText = '';

    let longestPeriod, longestPeriodTime = 0;
    data.periods.forEach(period => {
      const startDate = new Date(period.startDate);
      const endDate = new Date(period.endDate);
      if(endDate.getTime() - startDate.getTime() > longestPeriodTime) {
        longestPeriod = period;
        longestPeriodTime = endDate.getTime() - startDate.getTime();
      }
    });

    if(!longestPeriod) {
      statesEl.innerText = 'No data available';
    } else {
      const startDate = new Date(longestPeriod.startDate);
      const endDate = new Date(longestPeriod.endDate);

      const dimensions = {
        operatingSystem: 'Operating System',
        browser: 'Browser',
      };

      longestPeriod.dimensions.forEach(({ name, results }) => {
        if(!dimensions[name]) { return; }

        const headingEl = document.createElement('h3')
        headingEl.innerText = `Device usage grouped by ${dimensions[name]}`;
        statesEl.appendChild(headingEl);

        const periodDescEl = document.createElement('p');
        periodDescEl.innerText = (
          `This data covers the period from ${startDate.toLocaleDateString()} ` +
          `to ${endDate.toLocaleDateString()}.`
        );
        statesEl.appendChild(periodDescEl);

        const tableEl = document.createElement('table');
        const tableHeadEl = document.createElement('thead');
        const tableHeadRowEl = document.createElement('tr');
        [dimensions[name], 'Proportion'].forEach(heading => {
          const thEl = document.createElement('th')
          thEl.innerText = heading;
          tableHeadRowEl.appendChild(thEl);
        });
        tableHeadEl.appendChild(tableHeadRowEl);
        tableEl.appendChild(tableHeadEl);

        const tableBodyEl = document.createElement('tbody');
        results.forEach(({ dimensionValue, proportion }) => {
          const resultRowEl = document.createElement('tr');

          const dimValEl = document.createElement('td');
          dimValEl.innerText = dimensionValue;
          resultRowEl.appendChild(dimValEl);

          const proportionEl = document.createElement('td');
          proportionEl.style.textAlign = 'right';
          proportionEl.innerText = `${(proportion * 100).toFixed(2)}%`;
          resultRowEl.appendChild(proportionEl);

          tableBodyEl.appendChild(resultRowEl);
        });
        tableEl.appendChild(tableBodyEl);

        const divEl = document.createElement('div');
        divEl.appendChild(tableEl);
        statesEl.appendChild(divEl);
      });
    }
  } catch (e) {
    console.error(e);
    statesEl.innerText = 'Something went wrong fetching the data.';
  }
})();
</script>
