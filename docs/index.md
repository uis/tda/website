---
title: Overview
---

# Technology Design

## Starting points

<div class="card-container">
  <a href="guidance/cloud-first-steps/" class="card">
    <div class="card-icon">
      <i class="material-icons">
        school
      </i>
    </div>
    <div class="card-content">
      <h3>Cloud First Steps</h3>
      Get up and running with the Cloud
    </div>
  </a>
  <a href="standards/cloud-first/" class="card">
    <div class="card-icon">
      <i class="material-icons">
        cloud
      </i>
    </div>
    <div class="card-content">
      <h3>Cloud First</h3>
      A discussion of the "Cloud First" policy
    </div>
  </a>
  <a href="guidance/api/" class="card">
    <div class="card-icon">
      <i class="material-icons">
        assignment
      </i>
    </div>
    <div class="card-content">
      <h3>API Guidance</h3>
      Guidance on procuring and implementing API services
    </div>
  </a>
  <a href="guidance/hands-on-google-cloud/" class="card">
    <div class="card-icon">
      <i class="material-icons">
        build
      </i>
    </div>
    <div class="card-content">
      <h3>Hands on with Google Cloud</h3>
      A tutorial on hosting an application using Google Cloud
    </div>
  </a>
</div>


## This website

The source code of this website can be found in a
[dedicated project](https://gitlab.developers.cam.ac.uk/uis/tda/website).
